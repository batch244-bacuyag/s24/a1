let base = 2;
let exponent = 3;
const cubeRoot = (base, exponent) => Math.pow(base, exponent);
let getCube = cubeRoot(base,exponent);
console.log(`The cube of ${base} is ${getCube}`);

const fullAddress = {
	street: "258 Washington Ave NW",
	state: "Califronia",
	zipCode: "90011"
}

const {street, state, zipCode} = fullAddress
console.log(`I live at ${street}, ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}

const {name, type, weight, length} = animal
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);



const numbers = [1 , 2, 3, 4, 5];

numbers.forEach((number)=>{
	console.log(`${number}`);
});

const reduceNumber = numbers.reduce((x,y) => x+y, );

console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const newDog = new Dog("Patrash", 3, "Labrador" );
console.log(newDog);

